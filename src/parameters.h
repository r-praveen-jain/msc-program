/*
 * parameters.h
 *
 *  Created on: Feb 27, 2015
 *      Author: praveen
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

// Defines related to the controller
//#define DEBUG_MODE				// Connects to localhost instead of the drone
#define MANUAL_MODE				//FIXME: unused can be removed... Uses keyboard to control the drone, comment to use MPC
//#define PRINT_DEBUG_INFO		// Prints information at various stages of the code
#define USE_STATE_MACHINE
#define USE_TELEMETRY
//#define STEP_PATH
#define SQUARE_PATH
// Controller Types
#define PID_CONTROLLER		0
#define MPC_CONTROLLER		1
#define LQI_CONTROLLER		2
//#define CONTROLLER_TYPE 	PID_CONTROLLER
#define CONTROLLER_TYPE 	MPC_CONTROLLER
//#define CONTROLLER_TYPE		LQI_CONTROLLER
// Defines for UDP Communication with the Drone
#define MAIN_FREQ				100					// Frequency in Hertz
#define LOCALHOST				"127.0.0.1"

#define CONTROL_PORT			5555				// Port number for control commands
#define TELEMETRY_PORT			5556

#ifdef DEBUG_MODE
#define DRONE_IP	LOCALHOST
#define LAPTOP_IP	LOCALHOST
#else
#define DRONE_IP	"192.168.1.1"
#define LAPTOP_IP	"192.168.1.2"
#endif


#define FMAX				  8	 	// Maximum Thrust  = 8 N
#define FMIN				  0.4	// Minimum Thrust = 0.4 N

#define PHI_MIN				-5     // Minimum roll in degrees
#define PHI_MAX				 5    // Maximum roll in degrees

#define THETA_MIN			-5   // Minimum roll in degrees
#define THETA_MAX			 5    // Maximum roll in degrees

#define PSI_MIN				-5     // Minimum roll in degrees
#define PSI_MAX				 5     // Maximum roll in degrees
#define PSI_REF				  0	 	// Set reference yaw to zero

// Limit the flight area
#define XMIN		-3.0
#define XMAX		 2.0
#define YMIN        -3.0
#define YMAX		 2.6
#define ZMIN         0.3
#define ZMAX         3.0


#ifdef USE_TELEMETRY
#define NUM_THREADS			  3
#else
#define NUM_THREADS			  2
#endif


#define NUM_STATES			  6
#define NUM_INPUTS			  4
#define MAX_CMD_SIZE		  12

/* Thrust Calculations */
#define MASS					0.451
#define GRAVITY					9.8
#define PPRZ_THRUST_MAX			8700
#define PPRZ_THRUST_MIN			0

#define PPRZ_MAX				9600
#define PPRZ_MIN			   -9600

// Polynomial coefficients for conversion from thrust to PPRZ units
#define P3		 -71.2000
#define P2		1751.0000
#define P1		-497.5000

#define HOVER_ALTITUDE	1.5 // in meters
#define ON 	0xFF
#define OFF 0

#define CATASTROPHIC_BAT_LEVEL 9.3
#define CRITIC_BAT_LEVEL 9.6
#define LOW_BAT_LEVEL 9.7
#define MAX_BAT_LEVEL 12.4

// Waypoints

#ifdef SQUARE_PATH
#define NB_WAYPOINTS 5
#define WAYPOINTS {\
	{-1.5,-1.0,2.5},\
	{0.75,-1.0,2.5},\
	{0.75,1.0,2.5},\
	{-1.5,1.0,2.5},\
	{-1.5,-1.0,2.5}}
#endif

#ifdef STEP_PATH
#define NB_WAYPOINTS 2
#define WAYPOINTS {\
	{-1.5,-1.0,2.0},\
	{0.0,0.0,2.0}}
#endif

#endif /* PARAMETERS_H_ */
