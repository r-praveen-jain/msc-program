/*
 * statemachine.h
 *
 *  Created on: Mar 23, 2015
 *      Author: praveen
 */

#ifndef STATEMACHINE_H_
#define STATEMACHINE_H_

#define NUM_SM_EVENTS			7
#define NUM_SM_STATES			5

typedef enum SM_EVENT{
	EVENT_NIL,
	EVENT_INIT,
	EVENT_TAKEOFF,
	EVENT_MAN,
	EVENT_AUTO,
	EVENT_LAND,
	EVENT_KILL
} SM_EVENT;

typedef enum SM_STATE{
	STATE_OFF,
	STATE_IDLE,
	STATE_HOVER,
	STATE_AUTO,
	STATE_MAN
} SM_STATE;

typedef void (*SM_TFP)(); // SM_TFP - State Machine Transition Function Pointer (needed only for internal purposes

// Typedef for structure which is used as state transition table
typedef struct SM_TRANSITION{
	SM_STATE next_sm_state;
	SM_TFP	 action;
}SM_TRANSITION;

extern SM_STATE sm_state;
extern SM_EVENT sm_event;
extern SM_TRANSITION sm_transition_table [NUM_SM_EVENTS][NUM_SM_STATES];

void SM_Init(void);
void SM_Takeoff(void);
void SM_Hover2Auto(void);
void SM_Hover2Man(void);
void SM_Auto2Man(void);
void SM_Man2Auto(void);
void SM_land(void);
void SM_Kill(void);
void SM_Null(void);

#endif /* STATEMACHINE_H_ */
