/*
 * thread_callbacks.h
 *
 *  Created on: Mar 9, 2015
 *      Author: praveen
 */

#ifndef THREAD_CALLBACKS_H_
#define THREAD_CALLBACKS_H_

#include <stdbool.h>
#include <pthread.h>

extern volatile bool exit_flag;
extern volatile bool event_flag;
extern volatile bool low_bat;
extern pthread_rwlock_t state_rwlock;

void *Keyboard_thread(void);
void *Controller_thread(void);
void *Optitrack_thread(void);

#endif /* THREAD_CALLBACKS_H_ */
