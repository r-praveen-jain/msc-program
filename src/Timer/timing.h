/* Off-board controller
 * timing.h
 *
 *  Created on: February, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Timer functionalities (Not well developed/tested, could be useful for future work) 
 */

#ifndef TIMING_H_
#define TIMING_H_

#include <sys/time.h>
#include <stdbool.h>
extern struct itimerval main_timer;
extern struct timespec delay;
extern struct timeval tic_timer, toc_timer;
extern struct timeval timestamp;
//extern volatile bool event_flag;

int init_timer();
int set_timer();
int delay_ns(long nanosec);
int delay_us(long microsec);
int delay_ms(unsigned int millisec);
int delay_sec(unsigned char sec);
int close_timer();
int tic();
unsigned int toc();
unsigned long GetTimeStamp(void);


#endif /* TIMING_H_ */
