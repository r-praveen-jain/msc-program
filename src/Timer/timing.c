/* Off-board controller
 * timing.c
 *
 *  Created on: February, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Timer functionalities (Not well developed/tested, could be useful for future work) 
 */

#include "timing.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "../parameters.h"

struct itimerval main_timer;
struct timespec delay;
struct timeval tic_timer, toc_timer;
struct timeval timestamp;

// Initialize the timer for executing the controller at frequency MAIN_FREQ
int init_timer(){
	printf("Initializing Timer...\n");
	long long freq_microsec = (1000000/MAIN_FREQ);
	// Timer structure for executing loop at MAIN_FREQ Hz
	main_timer.it_value.tv_sec     =  freq_microsec / 1000000;
	main_timer.it_value.tv_usec    =  freq_microsec;
	main_timer.it_interval.tv_sec  =  main_timer.it_value.tv_sec;
	main_timer.it_interval.tv_usec =  main_timer.it_value.tv_usec;
	printf("Timer initialized\n");
	return EXIT_SUCCESS;
}

int set_timer(){
	char res;
	printf("Setting Timer\n");
	res = setitimer(ITIMER_REAL, &main_timer, NULL);
		if(res != 0) {
			perror("Failed to set timer");
			exit(EXIT_FAILURE);
		}
	printf("Timer Set! \n");
	return EXIT_SUCCESS;
}

// Problems with delay functions -- needs rework
int delay_ns(long nanosec){
	char res;
	delay.tv_sec = 0;
	delay.tv_nsec = nanosec;
	res = nanosleep(&delay,NULL);
	if(res == -1){
		perror("Delay interrupted");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

int delay_us(long microsec){
	char res;
	delay.tv_sec = 0;
	delay.tv_nsec = microsec*1000;
	res = nanosleep(&delay,NULL);
	if(res == -1){
		perror("Delay interrupted");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

int delay_ms(unsigned int millisec){
	char res;
	delay.tv_sec = millisec*1000000/1000000000;
	delay.tv_nsec = millisec*1000000;
	res = nanosleep(&delay,NULL);
	if(res == -1){
		perror("Delay interrupted");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

int delay_sec(unsigned char sec){
	char res;
	delay.tv_sec = sec;
	delay.tv_nsec = 0;
	res = nanosleep(&delay,NULL);
	if(res == -1){
		perror("Delay interrupted");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}


// Close all initialized timers
int close_timer(){
	printf("Clearing the Timer...\n");
	main_timer.it_value.tv_sec     =  0;
	main_timer.it_value.tv_usec    =  0;
	main_timer.it_interval.tv_sec  =  main_timer.it_value.tv_sec;
	main_timer.it_interval.tv_usec =  main_timer.it_value.tv_usec;
	printf("Timer Cleared\n");
	return EXIT_SUCCESS;
}


// tic and toc functions - similar to matlab interface (need to be tested)
int tic(){
	gettimeofday(&tic_timer, NULL);
	return EXIT_SUCCESS;
}

// tic and toc functions - similar to matlab interface (need to be tested)
unsigned int toc(){
	unsigned int elapsedTime;
	gettimeofday(&toc_timer, NULL);
	// compute and print the elapsed time in millisec
	elapsedTime  =  (toc_timer.tv_sec   -   tic_timer.tv_sec) * 1000000;      // sec to us
	elapsedTime +=  (toc_timer.tv_usec  -   tic_timer.tv_usec);   // us

	tic_timer.tv_sec = 0;
	tic_timer.tv_usec = 0;
	toc_timer.tv_sec = 0;
	toc_timer.tv_usec = 0;

    return elapsedTime;
}

// Time stamp to be used in file logger
unsigned long GetTimeStamp(){
	gettimeofday(&timestamp,NULL);
	unsigned long time_in_micros = 1000000 * timestamp.tv_sec + timestamp.tv_usec;
	return time_in_micros;
}
