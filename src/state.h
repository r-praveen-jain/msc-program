/*
 * state.h
 *
 *  Created on: Apr 11, 2015
 *      Author: praveen
 */

#ifndef STATE_H_
#define STATE_H_

#include "mpc_math.h"
#include <stdint.h>

typedef struct state_t{
	DoubleVect3 pos;
	DoubleVect3 vel;
	DoubleVect3 pos_est;
	DoubleVect3 vel_est;
	DoubleVect2 x_phi;
	DoubleVect2 x_theta;
	DoubleEulers att;
	DoubleRates rate;
	DoubleEulers att_sp;
	DoubleEulers stab_cmd;
	float vsupply;
	double optitrack_heading;
	uint8_t measure_flag;
}state_t;

extern state_t state;

// TODO: Integrate this with the code... Make a better interface to access states of the system.
// NOTE: All the function prototypes mentioned below are unused and undefined. Left for further development of better state interface.
/* State Get functions */
DoubleVect3 *stateGetPosition(void);
DoubleVect3 *stateGetVelocity(void);
DoubleEulers *stateGetAttitude(void);
DoubleRates *stateGetRate(void);

/* State Set functions*/
int stateSetPosition(DoubleVect3 *pos);
int stateSetVelocity(DoubleVect3 *vel);
int stateSetAttitude(DoubleEulers *att);
int stateSetRates(DoubleRates *rate);

int state_init(void);

#endif /* STATE_H_ */
