/*
 * Controller.h
 *
 *  Created on: Mar 9, 2015
 *      Author: praveen
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <stdbool.h>
#include <stdint.h>
#include "include/MPC_quadrotor.h" // Include File obtained from the FORCES PRO
#include "../parameters.h"
#include "../mpc_math.h"

// MACRO Definitions
#define PI					3.14159
#define RAD2DEG(x)	        ((x)*180/3.14159)
#define DEG2RAD(x)			((x)*3.14159/180)

//Defines used when transmitting control commands over UDP
#define START_BYTE 			0xFF
#define CTRL_CMD_BYTE		0xFE
#define MOTORS_ON_BYTE		0xFF


typedef struct PIDGains_t{
	double p;
	double i;
	double d;
} PIDGains_t;

// Globals required by the MPC controller
extern MPC_quadrotor_params mpc_params;
extern MPC_quadrotor_output mpc_output;
extern MPC_quadrotor_info   mpc_info;

// Globals needed by PID Controller
extern PIDGains_t gains;

extern DoubleVect3 pos, vel; // position and velocity of quadrotor
extern DoubleVect3 pos_ref;  // position reference of the quadrotor
extern DoubleVect3 pos_err;  // position error
extern DoubleVect3 int_err;  // Integral Error
extern DoubleVect3 waypoints[NB_WAYPOINTS];
extern const DoubleVect3 u_eq;	 // Control input at equilibrium point
extern DoubleEulers att;
extern DoubleRates	rate;

extern double heading;
extern float vsupply;

extern double ControlCommand[NUM_INPUTS];
extern uint16_t seqnum;

int init_controller(void);
inline int estimate_state(void);
inline int execute_controller(void);
inline int hover_controller(void);
inline int SendControlCommand(double *ctrl_cmd);
inline int SetMotorsOn(unsigned char status);
inline int SendLandCommand(void);


#endif /* CONTROLLER_H_ */
