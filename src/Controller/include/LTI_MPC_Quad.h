/*
LTI_MPC_Quad : A fast customized optimization solver.

Copyright (C) 2013-2015 EMBOTECH GMBH [info@embotech.com]. All rights reserved.


This software is intended for simulation and testing purposes only. 
Use of this software for any commercial purpose is prohibited.

This program is distributed in the hope that it will be useful.
EMBOTECH makes NO WARRANTIES with respect to the use of the software 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. 

EMBOTECH shall not have any liability for any damage arising from the use
of the software.

This Agreement shall exclusively be governed by and interpreted in 
accordance with the laws of Switzerland, excluding its principles
of conflict of laws. The Courts of Zurich-City shall have exclusive 
jurisdiction in case of any dispute.

*/

#include <stdio.h>

#ifndef __LTI_MPC_Quad_H__
#define __LTI_MPC_Quad_H__


/* DATA TYPE ------------------------------------------------------------*/
typedef double LTI_MPC_Quad_FLOAT;

typedef double LTI_MPC_QuadINTERFACE_FLOAT;

/* SOLVER SETTINGS ------------------------------------------------------*/
/* print level */
#ifndef LTI_MPC_Quad_SET_PRINTLEVEL
#define LTI_MPC_Quad_SET_PRINTLEVEL    (0)
#endif

/* timing */
#ifndef LTI_MPC_Quad_SET_TIMING
#define LTI_MPC_Quad_SET_TIMING    (1)
#endif

/* Numeric Warnings */
/* #define PRINTNUMERICALWARNINGS */

/* maximum number of iterations  */
#define LTI_MPC_Quad_SET_MAXIT         (20)	

/* scaling factor of line search (affine direction) */
#define LTI_MPC_Quad_SET_LS_SCALE_AFF  (LTI_MPC_Quad_FLOAT)(0.9)      

/* scaling factor of line search (combined direction) */
#define LTI_MPC_Quad_SET_LS_SCALE      (LTI_MPC_Quad_FLOAT)(0.95)  

/* minimum required step size in each iteration */
#define LTI_MPC_Quad_SET_LS_MINSTEP    (LTI_MPC_Quad_FLOAT)(1E-08)

/* maximum step size (combined direction) */
#define LTI_MPC_Quad_SET_LS_MAXSTEP    (LTI_MPC_Quad_FLOAT)(0.995)

/* desired relative duality gap */
#define LTI_MPC_Quad_SET_ACC_RDGAP     (LTI_MPC_Quad_FLOAT)(0.0001)

/* desired maximum residual on equality constraints */
#define LTI_MPC_Quad_SET_ACC_RESEQ     (LTI_MPC_Quad_FLOAT)(1E-06)

/* desired maximum residual on inequality constraints */
#define LTI_MPC_Quad_SET_ACC_RESINEQ   (LTI_MPC_Quad_FLOAT)(1E-06)

/* desired maximum violation of complementarity */
#define LTI_MPC_Quad_SET_ACC_KKTCOMPL  (LTI_MPC_Quad_FLOAT)(1E-06)


/* RETURN CODES----------------------------------------------------------*/
/* solver has converged within desired accuracy */
#define LTI_MPC_Quad_OPTIMAL      (1)

/* maximum number of iterations has been reached */
#define LTI_MPC_Quad_MAXITREACHED (0)

/* no progress in line search possible */
#define LTI_MPC_Quad_NOPROGRESS   (-7)




/* PARAMETERS -----------------------------------------------------------*/
/* fill this with data before calling the solver! */
typedef struct LTI_MPC_Quad_params
{
    /* vector of size 18 */
    LTI_MPC_Quad_FLOAT z1[18];

} LTI_MPC_Quad_params;


/* OUTPUTS --------------------------------------------------------------*/
/* the desired variables are put here by the solver */
typedef struct LTI_MPC_Quad_output
{
    /* vector of size 3 */
    LTI_MPC_Quad_FLOAT u1[3];

} LTI_MPC_Quad_output;


/* SOLVER INFO ----------------------------------------------------------*/
/* diagnostic data from last interior point step */
typedef struct LTI_MPC_Quad_info
{
    /* iteration number */
    int it;
	
    /* inf-norm of equality constraint residuals */
    LTI_MPC_Quad_FLOAT res_eq;
	
    /* inf-norm of inequality constraint residuals */
    LTI_MPC_Quad_FLOAT res_ineq;

    /* primal objective */
    LTI_MPC_Quad_FLOAT pobj;	
	
    /* dual objective */
    LTI_MPC_Quad_FLOAT dobj;	

    /* duality gap := pobj - dobj */
    LTI_MPC_Quad_FLOAT dgap;		
	
    /* relative duality gap := |dgap / pobj | */
    LTI_MPC_Quad_FLOAT rdgap;		

    /* duality measure */
    LTI_MPC_Quad_FLOAT mu;

	/* duality measure (after affine step) */
    LTI_MPC_Quad_FLOAT mu_aff;
	
    /* centering parameter */
    LTI_MPC_Quad_FLOAT sigma;
	
    /* number of backtracking line search steps (affine direction) */
    int lsit_aff;
    
    /* number of backtracking line search steps (combined direction) */
    int lsit_cc;
    
    /* step size (affine direction) */
    LTI_MPC_Quad_FLOAT step_aff;
    
    /* step size (combined direction) */
    LTI_MPC_Quad_FLOAT step_cc;    

	/* solvertime */
	LTI_MPC_Quad_FLOAT solvetime;   

} LTI_MPC_Quad_info;



/* SOLVER FUNCTION DEFINITION -------------------------------------------*/
/* examine exitflag before using the result! */
int LTI_MPC_Quad_solve(LTI_MPC_Quad_params* params, LTI_MPC_Quad_output* output, LTI_MPC_Quad_info* info, FILE* fs);



#endif