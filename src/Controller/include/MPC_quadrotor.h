/*
MPC_quadrotor : A fast customized optimization solver.

Copyright (C) 2013-2015 EMBOTECH GMBH [info@embotech.com]. All rights reserved.


This software is intended for simulation and testing purposes only. 
Use of this software for any commercial purpose is prohibited.

This program is distributed in the hope that it will be useful.
EMBOTECH makes NO WARRANTIES with respect to the use of the software 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. 

EMBOTECH shall not have any liability for any damage arising from the use
of the software.

This Agreement shall exclusively be governed by and interpreted in 
accordance with the laws of Switzerland, excluding its principles
of conflict of laws. The Courts of Zurich-City shall have exclusive 
jurisdiction in case of any dispute.

*/

#include <stdio.h>

#ifndef __MPC_quadrotor_H__
#define __MPC_quadrotor_H__


/* DATA TYPE ------------------------------------------------------------*/
typedef double MPC_quadrotor_FLOAT;

typedef double MPC_quadrotorINTERFACE_FLOAT;

/* SOLVER SETTINGS ------------------------------------------------------*/
/* print level */
#ifndef MPC_quadrotor_SET_PRINTLEVEL
#define MPC_quadrotor_SET_PRINTLEVEL    (0)
#endif

/* timing */
#ifndef MPC_quadrotor_SET_TIMING
#define MPC_quadrotor_SET_TIMING    (1)
#endif

/* Numeric Warnings */
/* #define PRINTNUMERICALWARNINGS */

/* maximum number of iterations  */
#define MPC_quadrotor_SET_MAXIT         (20)	

/* scaling factor of line search (affine direction) */
#define MPC_quadrotor_SET_LS_SCALE_AFF  (MPC_quadrotor_FLOAT)(0.9)      

/* scaling factor of line search (combined direction) */
#define MPC_quadrotor_SET_LS_SCALE      (MPC_quadrotor_FLOAT)(0.95)  

/* minimum required step size in each iteration */
#define MPC_quadrotor_SET_LS_MINSTEP    (MPC_quadrotor_FLOAT)(1E-08)

/* maximum step size (combined direction) */
#define MPC_quadrotor_SET_LS_MAXSTEP    (MPC_quadrotor_FLOAT)(0.995)

/* desired relative duality gap */
#define MPC_quadrotor_SET_ACC_RDGAP     (MPC_quadrotor_FLOAT)(0.0001)

/* desired maximum residual on equality constraints */
#define MPC_quadrotor_SET_ACC_RESEQ     (MPC_quadrotor_FLOAT)(1E-06)

/* desired maximum residual on inequality constraints */
#define MPC_quadrotor_SET_ACC_RESINEQ   (MPC_quadrotor_FLOAT)(1E-06)

/* desired maximum violation of complementarity */
#define MPC_quadrotor_SET_ACC_KKTCOMPL  (MPC_quadrotor_FLOAT)(1E-06)


/* RETURN CODES----------------------------------------------------------*/
/* solver has converged within desired accuracy */
#define MPC_quadrotor_OPTIMAL      (1)

/* maximum number of iterations has been reached */
#define MPC_quadrotor_MAXITREACHED (0)

/* no progress in line search possible */
#define MPC_quadrotor_NOPROGRESS   (-7)




/* PARAMETERS -----------------------------------------------------------*/
/* fill this with data before calling the solver! */
typedef struct MPC_quadrotor_params
{
    /* vector of size 26 */
    MPC_quadrotor_FLOAT z1[26];

} MPC_quadrotor_params;


/* OUTPUTS --------------------------------------------------------------*/
/* the desired variables are put here by the solver */
typedef struct MPC_quadrotor_output
{
    /* vector of size 3 */
    MPC_quadrotor_FLOAT u1[3];

} MPC_quadrotor_output;


/* SOLVER INFO ----------------------------------------------------------*/
/* diagnostic data from last interior point step */
typedef struct MPC_quadrotor_info
{
    /* iteration number */
    int it;
	
    /* inf-norm of equality constraint residuals */
    MPC_quadrotor_FLOAT res_eq;
	
    /* inf-norm of inequality constraint residuals */
    MPC_quadrotor_FLOAT res_ineq;

    /* primal objective */
    MPC_quadrotor_FLOAT pobj;	
	
    /* dual objective */
    MPC_quadrotor_FLOAT dobj;	

    /* duality gap := pobj - dobj */
    MPC_quadrotor_FLOAT dgap;		
	
    /* relative duality gap := |dgap / pobj | */
    MPC_quadrotor_FLOAT rdgap;		

    /* duality measure */
    MPC_quadrotor_FLOAT mu;

	/* duality measure (after affine step) */
    MPC_quadrotor_FLOAT mu_aff;
	
    /* centering parameter */
    MPC_quadrotor_FLOAT sigma;
	
    /* number of backtracking line search steps (affine direction) */
    int lsit_aff;
    
    /* number of backtracking line search steps (combined direction) */
    int lsit_cc;
    
    /* step size (affine direction) */
    MPC_quadrotor_FLOAT step_aff;
    
    /* step size (combined direction) */
    MPC_quadrotor_FLOAT step_cc;    

	/* solvertime */
	MPC_quadrotor_FLOAT solvetime;   

} MPC_quadrotor_info;



/* SOLVER FUNCTION DEFINITION -------------------------------------------*/
/* examine exitflag before using the result! */
int MPC_quadrotor_solve(MPC_quadrotor_params* params, MPC_quadrotor_output* output, MPC_quadrotor_info* info, FILE* fs);



#endif