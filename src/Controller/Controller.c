/*
 * Controller.c
 *
 *  Created on: Mar 9, 2015
 *      Author: praveen
 */

#include "Controller.h"
#include "../Communication/Control_COMport.h"
#include "../parameters.h"
#include "../state.h"
#include "KalmanFilter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//---------------------------------------------------------------------------------
// Declaration of MACROS and variables needed for kalman filter implementation
#define KF_NUM_STATES 10
#define KF_NUM_INPUTS 3
#define KF_NUM_MEASURE 5

double Q[KF_NUM_STATES] = {1, 1, 1, 100, 100, 100, 0.00001, 0.00001, 0.00001, 0.00001};
double R[KF_NUM_MEASURE] = {0.1000, 0.1000, 0.1000, 0.0001, 0.0001};
double P[KF_NUM_STATES] = {1,1,1,1,1,1,1,1,1,1};
kf_params_t kf_params = {.num_states = KF_NUM_STATES,
						 .num_inputs = KF_NUM_INPUTS,
						 .num_measure = KF_NUM_MEASURE,
						 .process_cov = Q,
						 .measure_cov = R,
						 .state_cov = P
};

double A[KF_NUM_STATES][KF_NUM_STATES] = {{1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
										  {0.0000, 1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000,-0.2513,-0.0016},
										  {0.0000, 0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.1837, 0.0007, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.9809, 0.0131, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-0.0653, 0.9852, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.9781, 0.0139},
										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-0.0569, 1.0025},
};

//double A[KF_NUM_STATES][KF_NUM_STATES] = {{1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
//										  {0.0000, 1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0100, 0.0000, 0.0000, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.2513, 0.0016},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.1837, 0.0007, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.9809, 0.0131, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-0.0653, 0.9852, 0.0000, 0.0000},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.9781, 0.0139},
//										  {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-0.0569, 1.0025},
//};



double B[KF_NUM_STATES][KF_NUM_INPUTS] = {{0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000},
										  {0.0000, 0.0000, 0.0000},
										  {0.0100, 0.0000, 0.0000},
										  {0.0000,-0.0056, 0.0000},
										  {0.0000,-0.0273, 0.0000},
										  {0.0000, 0.0000,-0.0045},
										  {0.0000, 0.0000,-0.0122},
};
double C[KF_NUM_MEASURE][KF_NUM_STATES] = {{1.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
									 	   {0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
									 	   {0.0000, 0.0000, 1.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000},
									 	   {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-1.8740,-0.0071, 0.0000, 0.0000},
									 	   {0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000,-2.5640,-0.0167},
};

//----------------------------------------------------------------------------------------------
/* LQ Controller gain*/
#define LQ_NUM_STATES 13
#define LQ_NUM_INPUTS 3

//Qx = diag([10,10,10,0.01,0.01,0.01,0.01,0.01,0.01,0.01]); R = diag([1,1000,1000])*eye(nu);
double LQGain[LQ_NUM_INPUTS][LQ_NUM_STATES] = {{0.0000, 0.0000, 5.5354, 0.0000, 0.0000, 3.3420, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0311},
											   {0.0000,-0.2706, 0.0000, 0.0000,-0.3188, 0.0000,-0.9494,-0.5054, 0.0000, 0.0000,-0.0000,-0.0010, 0.0000},
											   {0.2760, 0.0000, 0.0000, 0.3334, 0.0000, 0.0000, 0.0000, 0.0000,-0.6144,-1.2141, 0.0010, 0.0000, 0.0000},
};

////Qx = diag([10,10,10,0.0,0.0,0.0,0.0,0.0,0.0,0.0]); R = diag([1,10000,10000])*eye(nu);
//double LQGain[LQ_NUM_INPUTS][LQ_NUM_STATES] = {{0.0000, 0.0000, 5.5335, 0.0000, 0.0000, 3.3403, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0311},
//											   {0.0000,-0.1159, 0.0000, 0.0000,-0.1975, 0.0000,-0.5557,-0.3465, 0.0000, 0.0000,-0.0000,-0.0003, 0.0000},
//											   {-0.1202, 0.0000, 0.0000,-0.2136, 0.0000, 0.0000, 0.0000, 0.0000,-0.2049,-0.8648,-0.0003, 0.0000, 0.0000},
//};

//Qx = diag([10,10,10,0.0,0.0,0.0,0.0,0.0,0.0,0.0]); R = diag([1,100,100])*eye(nu);
//double LQGain[LQ_NUM_INPUTS][LQ_NUM_STATES] = {{ 0.0000, 0.0000, 5.5335, 0.0000, 0.0000, 3.3403, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0311},
//											   { 0.0000,-0.6652, 0.0000, 0.0000,-0.5510, 0.0000,-1.7377,-0.7377, 0.0000, 0.0000,-0.0000,-0.0031, 0.0000},
//											   {-0.6715, 0.0000, 0.0000,-0.5638, 0.0000, 0.0000, 0.0000, 0.0000,-1.7025,-1.6749,-0.0031, 0.0000, 0.0000},
//};

//Qx = diag([10,10,10,0.1,0.1,0.1,0.0,0.0,0.0,0.0]); R = diag([1,100,100])*eye(nu);
//double LQGain[LQ_NUM_INPUTS][LQ_NUM_STATES] = {{ 0.0000, 0.0000, 5.5430, 0.0000, 0.0000, 3.3577, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0311},
//											   { 0.0000,-0.6660, 0.0000, 0.0000,-0.5527, 0.0000,-1.7453,-0.7393, 0.0000, 0.0000,-0.0000,-0.0031, 0.0000},
//											   {-0.6723, 0.0000, 0.0000,-0.5656, 0.0000, 0.0000, 0.0000, 0.0000,-1.7151,-1.6774,-0.0031, 0.0000, 0.0000},
//};
/*---------------------------------------------------------------------------------*/

MPC_quadrotor_params mpc_params = {{0}};
MPC_quadrotor_output mpc_output;
MPC_quadrotor_info   mpc_info;

PIDGains_t gains = {.p = 1, .i = 0.0, .d = 0.0};


DoubleVect3 pos = {.x = 0, .y = 0, .z = 0};
DoubleVect3 vel = {.x = 0, .y = 0, .z = 0};
DoubleVect3 int_err = {.x = 0, .y = 0, .z = 0};  // Integral Error
DoubleVect3 hover_int_err = {.x = 0, .y = 0, .z = 0};  // Integral Error
DoubleVect3 pos_err = {.x = 0, .y = 0, .z = 0};  // Error in position
DoubleVect3 pos_ref = {.x = 0, .y = 0, .z = 0};  // In meters
DoubleVect3 zeros = {.x = 0, .y = 0, .z = 0};	 // dummy structure with zeros if needed
// TODO: A better interface to access the robot states hold in global structure "RobotState"
DoubleEulers att = {.phi = 0, .theta = 0, .psi = 0};
DoubleEulers att_ref = {.phi = 0, .theta = 0, .psi = 0};
DoubleRates	rate = {.p = 0, .q = 0, .r = 0};
const DoubleVect3 u_eq  = {.x = 0, .y = 0, .z = MASS*GRAVITY};	// Control input at equilibrium point

DoubleVect3 waypoints[NB_WAYPOINTS] = WAYPOINTS;
uint16_t seqnum = 0;
double ControlCommand[NUM_INPUTS];
double heading;
float vsupply;

//-----------------------------------------------------------------------
/* Static function definitions*/
//-----------------------------------------------------------------------

static inline void FILL_MPC_PARAMS(){
	mpc_params.z1[0] = pos_err.x;
	mpc_params.z1[1] = pos_err.y;
	mpc_params.z1[2] = pos_err.z;
	mpc_params.z1[3] = state.vel_est.x;
	mpc_params.z1[4] = state.vel_est.y;
	mpc_params.z1[5] = state.vel_est.z;
	mpc_params.z1[6] = state.x_phi.x;
	mpc_params.z1[7] = state.x_phi.y;
	mpc_params.z1[8] = state.x_theta.x;
	mpc_params.z1[9] = state.x_theta.y;
	mpc_params.z1[10] = int_err.x;
	mpc_params.z1[11] = int_err.y;
	mpc_params.z1[12] = int_err.z;
}

static inline void PI_Hover_Controller(){
	double inp[NUM_INPUTS];
	double thrust;
	VECT3_DIFF(pos_err, pos_ref, pos); // Note: error = reference - state
	VECT3_ADD(hover_int_err, pos_err);	   // integrate the error in position
	inp[0] = gains.p*pos_err.x + gains.i*hover_int_err.x;
	inp[1] = gains.p*pos_err.y + gains.i*hover_int_err.y;
	inp[2] = gains.p*pos_err.z + gains.i*hover_int_err.z;
	double psii = 0; // Zero at the moment or use -state.optitrack_heading
	thrust = MASS*inp[2] + MASS*GRAVITY;
	att_ref.phi = (1/GRAVITY)*(inp[0]*sin(psii) - inp[1]*cos(psii));
	att_ref.theta = (1/GRAVITY)*(inp[0]*cos(psii) + inp[1]*sin(psii));
	BOUND(thrust, FMAX, FMIN);
	BOUND(att_ref.phi, DEG2RAD(PHI_MAX), DEG2RAD(PHI_MIN));
	BOUND(att_ref.theta, DEG2RAD(THETA_MAX), DEG2RAD(THETA_MIN));
	ControlCommand[0] = thrust;
	ControlCommand[1] = att_ref.phi;	// roll remains unchanged in ENU to NED conversion
	ControlCommand[2] = -att_ref.theta; // -1 for ENU to NED
	ControlCommand[3] = state.optitrack_heading; // optitrack heading arrives in NED frame

}

static inline void LQI_Controller(){
	VECT3_DIFF(pos_err, state.pos_est, pos_ref);
	VECT3_ADD(int_err, pos_err);
	double x[LQ_NUM_STATES];
	double inp[LQ_NUM_INPUTS] = {0};
	uint8_t j;
	x[0] = pos_err.x;
	x[1] = pos_err.y;
	x[2] = pos_err.z;
	x[3] = state.vel_est.x;
	x[4] = state.vel_est.y;
	x[5] = state.vel_est.z;
	x[6] = state.x_phi.x;
	x[7] = state.x_phi.y;
	x[8] = state.x_theta.x;
	x[9] = state.x_theta.y;
	x[10] = int_err.x;
	x[11] = int_err.y;
	x[12] = int_err.z;

	for(j = 0; j < LQ_NUM_STATES; j++){
		inp[0] += -LQGain[0][j]*x[j];
		inp[1] += -LQGain[1][j]*x[j];
		inp[2] += -LQGain[2][j]*x[j];
	}
	double thrust = MASS*inp[0] + u_eq.z; //thrust
	double phi = inp[1] + u_eq.x; // roll
	double theta = -(inp[2] + u_eq.y); //pitch
	BOUND(thrust, FMAX, FMIN);
	BOUND(phi, DEG2RAD(PHI_MAX), DEG2RAD(PHI_MIN));
	BOUND(theta, DEG2RAD(THETA_MAX), DEG2RAD(THETA_MIN));
	ControlCommand[0] =  thrust;
	ControlCommand[1] =	 phi;
	ControlCommand[2] =  theta;
	ControlCommand[3] =  state.optitrack_heading; // send optitrack heading to the drone

}

//-----------------------------------------------------------------------
/* Function declarations */
//-----------------------------------------------------------------------

int init_controller(){
	bzero(&mpc_params, sizeof(mpc_params));
	bzero(&mpc_output, sizeof(mpc_output));
//	bzero(ControlCommand, NUM_INPUTS);
	init_kf(A,B,C);
	return EXIT_SUCCESS;
}

inline int estimate_state(){

//	pos.x = state.pos.x;
//	pos.y = state.pos.y;
//	pos.z = state.pos.z;
//	vel.x = state.vel.x;
//	vel.y = state.vel.y;
//	vel.z = state.vel.z;
//	att.phi = state.att.phi;
//	att.theta = state.att.theta;
//	att.psi = state.att.psi;
//	rate.p = state.rate.p;
//	rate.q = state.rate.q;
//	rate.r = state.rate.r;
	heading = state.optitrack_heading;
	vsupply = state.vsupply;


	double input[KF_NUM_INPUTS];
	double measure[KF_NUM_MEASURE];
	double xhat[KF_NUM_STATES];
//	input[0] = ControlCommand[0] - MASS*GRAVITY;
	input[0] = (ControlCommand[0] - MASS*GRAVITY)/(double) MASS;
	input[1] = ControlCommand[1];
	input[2] = -ControlCommand[2];
	measure[0] = pos.x;
	measure[1] = pos.y;
	measure[2] = pos.z;
	measure[3] = state.att.phi;
	measure[4] = -state.att.theta;
	kf_periodic(input,measure,xhat, state.measure_flag);
	state.pos_est.x = xhat[0];
	state.pos_est.y = xhat[1];
	state.pos_est.z = xhat[2];
	state.vel_est.x = xhat[3];
	state.vel_est.y = xhat[4];
	state.vel_est.z = xhat[5];
	state.x_phi.x   = xhat[6];
	state.x_phi.y   = xhat[7];
	state.x_theta.x = xhat[8];
	state.x_theta.y = xhat[9];
	if(state.measure_flag == 1){ state.measure_flag = 0;}
	return EXIT_SUCCESS;
}

inline int execute_controller(){

#if(CONTROLLER_TYPE == MPC_CONTROLLER)
	char res;
	VECT3_DIFF(pos_err, state.pos_est, pos_ref);
	VECT3_ADD(int_err, pos_err);
	FILL_MPC_PARAMS();
	res = MPC_quadrotor_solve(&mpc_params, &mpc_output, &mpc_info,0);
	double thrust = MASS*mpc_output.u1[0] + u_eq.z; //thrust
	BOUND(thrust, FMAX, FMIN);
	ControlCommand[0] =  thrust;
	ControlCommand[1] =  mpc_output.u1[1] + u_eq.x; // roll
	ControlCommand[2] =  -(mpc_output.u1[2] + u_eq.y); //pitch
	ControlCommand[3] =  state.optitrack_heading; // send optitrack heading to the drone
#ifdef PRINT_DEBUG_INFO
	if (res == 1){
		printf("Optimal Solution found with solve time %f\n", mpc_info.solvetime);
	}
	else {
		printf("FORCES quit with following exit code %d\n", res);
	}
#endif //-- End of PRINT_DEBUG_INFO

#elif(CONTROLLER_TYPE == PID_CONTROLLER)
	PID_Controller();
#elif(CONTROLLER_TYPE == LQI_CONTROLLER)
	LQI_Controller();
#endif
	return EXIT_SUCCESS;
}

inline int hover_controller(){
	PI_Hover_Controller();
	return EXIT_SUCCESS;
}
inline int SendControlCommand(double *ctrl_cmd){
	double thrust = (*ctrl_cmd);
	int16_t phi	  =  (*(ctrl_cmd+1))*1000;
	int16_t theta  = (*(ctrl_cmd+2))*1000;
	int16_t psi = (*(ctrl_cmd+3))*1000;

	// Local variables used to create the control command
	unsigned char cmd[MAX_CMD_SIZE];
	uint16_t PPRZ_thrust;

	// Calculation of thrust in PPRZ units (0 - 9600)
	PPRZ_thrust = P3*thrust*thrust + P2*thrust + P1;
	if(PPRZ_thrust > PPRZ_THRUST_MAX){
		PPRZ_thrust = PPRZ_THRUST_MAX;
	}else if(PPRZ_thrust <= PPRZ_THRUST_MIN){
		PPRZ_thrust = PPRZ_THRUST_MIN;
	}

	// Creating the Control Command
	seqnum++;
	cmd[0] = START_BYTE;
	cmd[1] = CTRL_CMD_BYTE;
	cmd[2] = (seqnum >> 8) & 0x00FF;			// seqnum_HB
	cmd[3] = seqnum & 0x00FF;					// seqnum_LB
	cmd[4] = (PPRZ_thrust >> 8) & 0x00FF; 		// PPRZ_thrust_HB
	cmd[5] = PPRZ_thrust & 0x00FF; 				// PPRZ_thrust_LB
	cmd[6] = (phi >> 8) & 0x00FF; 				// phi_HB
	cmd[7] = phi & 0x00FF;						// phi_LB
	cmd[8] = (theta >> 8) & 0x00FF;				// theta_HB
	cmd[9] = theta & 0x00FF;					// theta_LB
	cmd[10] = (psi >> 8) & 0x00FF;				// psi_HB
	cmd[11] = psi & 0x00FF;						// psi_LB

	SendPacket(cmd, MAX_CMD_SIZE);
	return EXIT_SUCCESS;
}

inline int SetMotorsOn(unsigned char status){
	unsigned char seqnum_LB, seqnum_HB;
	seqnum++;
	seqnum_LB = seqnum & 0x00FF;
	seqnum_HB = (seqnum >> 8) & 0x00FF;
	unsigned char cmd[MAX_CMD_SIZE] = {START_BYTE, MOTORS_ON_BYTE, seqnum_HB, seqnum_LB, status,0,0,0,0,0,0,0};
	SendPacket(cmd, MAX_CMD_SIZE);
	return EXIT_SUCCESS;
}

inline int SendLandCommand(){
	ControlCommand[0] = 0.95*MASS*GRAVITY;
	ControlCommand[1] = 0;
	ControlCommand[2] = 0;
	ControlCommand[3] = state.optitrack_heading;
	SendControlCommand(ControlCommand);
	return EXIT_SUCCESS;
}

