/* Author: praveen jain */

#include "KalmanFilter.h"
#include <stdint.h>
#include <gsl/gsl_blas.h> 
#include <gsl/gsl_linalg.h>

// Include these defines in parameters.h of my project
//#define NUM_STATES 6
//#define NUM_INPUTS 3
//#define NUM_MEASURE 3 TODO: Not needed, remove it.

kf_variables_t kf_variables;
static kf_temp_t kf_temp;

// Static functions
static inline void update_kf(uint8_t measure_flag){
	/* Time Update */
	// xhat_minus = A*xhat + B*u
	gsl_blas_dgemv(CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.A, (const gsl_vector *) kf_variables.xhat, 0.0, kf_variables.xhat_minus);
	gsl_blas_dgemv(CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.B, (const gsl_vector *) kf_variables.inputs, 1.0, kf_variables.xhat_minus);

	// P_minus = A*P*A'+ Q;
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.A, (const gsl_matrix *) kf_variables.P, 0.0, kf_temp.AP);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, (const gsl_matrix *) kf_temp.AP, (const gsl_matrix *) kf_variables.A, 0.0, kf_variables.P_minus);
	gsl_matrix_add(kf_variables.P_minus, kf_variables.Q);

	/* Measurement update */
	// K = P_minus*C'*inv(C*P_minus*C' + R);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans, 1.0, (const gsl_matrix *) kf_variables.P_minus, (const gsl_matrix *) kf_variables.C, 0.0, kf_temp.PCt);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.C, (const gsl_matrix *) kf_temp.PCt, 0.0, kf_temp.CPCt_R);
	gsl_matrix_add(kf_temp.CPCt_R, kf_variables.R);
	// Compute inverse
	gsl_linalg_cholesky_decomp(kf_temp.CPCt_R);
	gsl_linalg_cholesky_invert(kf_temp.CPCt_R);
	// Compute Kalman gain
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, (const gsl_matrix *) kf_temp.PCt, (const gsl_matrix *) kf_temp.CPCt_R, 0.0, kf_variables.K);

	// xhat = xhat_minus + measure_flag*K*(y - C*xhat_minus);
	gsl_blas_dgemv(CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.C, (const gsl_vector *) kf_variables.xhat_minus, 0.0, kf_temp.yhat);
	gsl_vector_sub(kf_variables.measure, (const gsl_vector *) kf_temp.yhat);

	gsl_blas_dgemv(CblasNoTrans, measure_flag, (const gsl_matrix *) kf_variables.K, (const gsl_vector *) kf_variables.measure, 0.0, kf_variables.xhat);
	gsl_vector_add(kf_variables.xhat, (const gsl_vector *) kf_variables.xhat_minus);

	// P = (I - K*C)*P_minus;
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.K, (const gsl_matrix *) kf_variables.C, 0.0, kf_temp.AP);
	gsl_matrix_sub(kf_variables.I, (const gsl_matrix *) kf_temp.AP);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, (const gsl_matrix *) kf_variables.I, (const gsl_matrix *) kf_variables.P_minus, 0.0, 	kf_variables.P);

	// Reset I to identity matrix and xhat_minus to zero -- needed for next iteration
	gsl_matrix_set_identity(kf_variables.I);
	gsl_vector_set_zero (kf_variables.xhat_minus);
}

static inline void updateSysMatrix(size_t nRows, size_t nCols, double mat[][nCols], gsl_matrix *gsl_mat){
	uint8_t i,j;
	for (i = 0; i < nRows; i++){
		for(j = 0; j < nCols; j++){
			gsl_matrix_set(gsl_mat, i, j, mat[i][j]);
		}
	}
}

static inline void setInput(double *input){
	uint8_t i = 0;
	for(i = 0;i < kf_params.num_inputs; i++){
		gsl_vector_set(kf_variables.inputs, i, *(input + i));
	}
}

static inline void setMeasurement(double *measure){
	uint8_t i = 0;
	for(i = 0;i < kf_params.num_measure; i++){
		gsl_vector_set(kf_variables.measure, i, *(measure + i));
	}
}

static inline void setCovMatrix(){
	uint8_t i;
	for(i = 0; i< kf_params.num_states; i++){
		gsl_matrix_set(kf_variables.Q, i,i, *(kf_params.process_cov + i));
		gsl_matrix_set(kf_variables.P, i,i, *(kf_params.state_cov + i));
	}
	for(i = 0; i< kf_params.num_measure; i++){
		gsl_matrix_set(kf_variables.R, i,i, *(kf_params.measure_cov + i));
	}
}

static inline void getStateEstimate(double *xhat){
	uint8_t i;
	for(i = 0; i < kf_params.num_states; i++){
		*(xhat+i) = gsl_vector_get(kf_variables.xhat, i);
	}
}

inline void init_kf(double A[][kf_params.num_states], double B[][kf_params.num_inputs], double C[][kf_params.num_states]){

	// Initialize kf_variables
	kf_variables.xhat_minus = gsl_vector_alloc(kf_params.num_states);
	gsl_vector_set_zero(kf_variables.xhat_minus);

	kf_variables.xhat       = gsl_vector_alloc(kf_params.num_states);
	gsl_vector_set_zero(kf_variables.xhat);

	kf_variables.inputs	= gsl_vector_alloc(kf_params.num_inputs);
	gsl_vector_set_zero(kf_variables.inputs);

	kf_variables.measure	= gsl_vector_alloc(kf_params.num_measure);
	gsl_vector_set_zero(kf_variables.measure);

	kf_variables.A		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);
	gsl_matrix_set_zero(kf_variables.A);

	kf_variables.B		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_inputs);
	gsl_matrix_set_zero(kf_variables.B);

	kf_variables.C 		= gsl_matrix_alloc(kf_params.num_measure, kf_params.num_states);
	gsl_matrix_set_zero(kf_variables.C);

	kf_variables.Q 		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);
	kf_variables.R 		= gsl_matrix_alloc(kf_params.num_measure, kf_params.num_measure);

	kf_variables.P_minus	= gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);
	gsl_matrix_set_zero(kf_variables.P_minus);

	kf_variables.P 		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);

	kf_variables.K		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_measure);
	gsl_matrix_set_zero(kf_variables.K);

	kf_variables.I		= gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);
	gsl_matrix_set_identity(kf_variables.I);
	gsl_matrix_set_identity(kf_variables.P);
	gsl_matrix_set_identity(kf_variables.Q);
	gsl_matrix_set_identity(kf_variables.R);

	updateSysMatrix(kf_params.num_states, kf_params.num_states, A ,kf_variables.A);
	updateSysMatrix(kf_params.num_states, kf_params.num_inputs, B, kf_variables.B);
	updateSysMatrix(kf_params.num_measure, kf_params.num_states, C, kf_variables.C);
	setCovMatrix();

	// Initialize kf_temp
	kf_temp.Ax = gsl_vector_alloc(kf_params.num_states);
	gsl_vector_set_zero(kf_temp.Ax);
	kf_temp.Bu = gsl_vector_alloc(kf_params.num_states);
	gsl_vector_set_zero(kf_temp.Bu);
	kf_temp.yhat = gsl_vector_alloc(kf_params.num_measure);
	gsl_vector_set_zero(kf_temp.yhat);
	kf_temp.AP = gsl_matrix_alloc(kf_params.num_states, kf_params.num_states);
	gsl_matrix_set_zero(kf_temp.AP);
	kf_temp.PCt = gsl_matrix_alloc(kf_params.num_states, kf_params.num_measure);
	gsl_matrix_set_zero(kf_temp.PCt);
	kf_temp.CPCt_R = gsl_matrix_alloc(kf_params.num_measure, kf_params.num_measure);
	gsl_matrix_set_zero(kf_temp.CPCt_R);
}

inline void kf_periodic(double *input, double *measure, double *xhat, uint8_t measure_flag){
	setInput(input);
	setMeasurement(measure);
	update_kf(measure_flag);
	getStateEstimate(xhat);
}

inline void clear_kf(){
	gsl_vector_free(kf_variables.xhat_minus);
	gsl_vector_free(kf_variables.xhat);
	gsl_vector_free(kf_variables.inputs);
	gsl_vector_free(kf_variables.measure);

	gsl_matrix_free(kf_variables.A);
	gsl_matrix_free(kf_variables.B);
	gsl_matrix_free(kf_variables.C);
	gsl_matrix_free(kf_variables.Q);
	gsl_matrix_free(kf_variables.R);
	gsl_matrix_free(kf_variables.P_minus);
	gsl_matrix_free(kf_variables.P);
	gsl_matrix_free(kf_variables.K);
	gsl_matrix_free(kf_variables.I);

	gsl_vector_free(kf_temp.Ax);
	gsl_vector_free(kf_temp.Bu);
	gsl_vector_free(kf_temp.yhat);

	gsl_matrix_free(kf_temp.AP);
	gsl_matrix_free(kf_temp.PCt);
	gsl_matrix_free(kf_temp.CPCt_R);
}

