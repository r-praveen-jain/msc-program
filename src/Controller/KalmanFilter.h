/*
 * KalmanFilter.h
 *
 *  Created on: May 1, 2015
 *      Author: praveen
 */

#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#define HAVE_INLINE

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

typedef struct kf_params_t{
	size_t num_states;
	size_t num_inputs;
	size_t num_measure;
	double *process_cov;
	double *measure_cov;
	double *state_cov;
} kf_params_t;

typedef struct kf_variables_t{
	gsl_vector *xhat_minus;
	gsl_vector *xhat;
	gsl_vector *inputs;
	gsl_vector *measure;
	gsl_matrix *A;
	gsl_matrix *B;
	gsl_matrix *C;
	gsl_matrix *Q;
	gsl_matrix *R;
	gsl_matrix *P_minus;
	gsl_matrix *P;
	gsl_matrix *K;
	gsl_matrix *I;
} kf_variables_t;

typedef struct kf_temp_t{
	gsl_vector *Ax;
	gsl_vector *Bu;
	gsl_vector *yhat;
	gsl_matrix *AP;
	gsl_matrix *PCt;
	gsl_matrix *CPCt_R;

} kf_temp_t;

extern kf_params_t kf_params;
extern kf_variables_t kf_variables;

extern inline void init_kf(double A[][kf_params.num_states], double B[][kf_params.num_inputs], double C[][kf_params.num_states]);
extern inline void kf_periodic(double *input, double *measure, double *xhat, uint8_t measure_flag);
extern inline void clear_kf(void);

#endif /* KALMANFILTER_H_ */
