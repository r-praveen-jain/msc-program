/*
 * Copyright (C) 2014 Freek van Tienen <freek.v.tienen@gmail.com>
 *
 * This file is part of paparazzi.
 *
 * paparazzi is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * paparazzi is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with paparazzi; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

/** @file modules/loggers/file_logger.c
 *  @brief File logger for Linux based autopilots
 */

#include "file_logger.h"

#include <stdio.h>
#include <stdint.h>
#include "../Controller/Controller.h"
#include "../Timer/timing.h"
#include "../state.h"
#include "../parameters.h"

#ifndef FILE_LOGGER_PATH
#if(CONTROLLER_TYPE == MPC_CONTROLLER)
#define FILE_LOGGER_PATH "/home/praveen/Desktop/logdata/mpc_quadrotor/mpc_quadrotor"
#elif (CONTROLLER_TYPE == LQI_CONTROLLER)
#define FILE_LOGGER_PATH "/home/praveen/Desktop/logdata/mpc_quadrotor/lqi_quadrotor"
#elif (CONTROLLER_TYPE == PID_CONTROLLER)
#define FILE_LOGGER_PATH "/home/praveen/Desktop/logdata/mpc_quadrotor/pid_quadrotor"
#endif
#endif
/** The file pointer */
static FILE *file_logger;
extern unsigned int timeCounter;
/** Start the file logger and open a new file */
void file_logger_start(void)
{
	uint32_t counter = 0;
	char filename[512];

	// Check for available files
	sprintf(filename, "%s%05d.csv", FILE_LOGGER_PATH, counter);
	while ((file_logger = fopen(filename, "r"))) {
		fclose(file_logger);

		counter++;
		sprintf(filename, "%s%05d.csv", FILE_LOGGER_PATH, counter);
	}

	file_logger = fopen(filename, "w");

	if (file_logger != NULL) {
		fprintf(file_logger, "timestamp, counter, x, y, z, vel_x, vel_y, vel_z, phi, theta, psi,\
				              p, q, r, thrust, phi_cmd, theta_cmd, stab_cmd_phi, stab_cmd_theta, stab_cmd_psi,\
				              sp_phi, sp_theta, sp_psi, opti_heading, \
				              x_est, y_est, z_est, vx_est, vy_est, vz_est,x_phi, y_phi, x_theta, y_theta, solvetime\n");
	}
}

/** Stop the logger an nicely close the file */
void file_logger_stop(void)
{
	fclose(file_logger);
	file_logger = NULL;
}

/** Log the values to a csv file */
void file_logger_periodic(void)
{
	if (file_logger == NULL) {
		return;
	}
	fprintf(file_logger, "%lu,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
			GetTimeStamp(),
			timeCounter,
			pos.x,pos.y,pos.z,
			vel.x,vel.y,vel.z,
			state.att.phi, state.att.theta, state.att.psi,
			state.rate.p, state.rate.q, state.rate.r,
			ControlCommand[0], ControlCommand[1], ControlCommand[2],
			state.stab_cmd.phi, state.stab_cmd.theta, state.stab_cmd.psi,
			state.att_sp.phi, state.att_sp.theta, state.att_sp.psi,
			state.optitrack_heading,
			state.pos_est.x,state.pos_est.y, state.pos_est.z,
			state.vel_est.x,state.vel_est.y, state.vel_est.z,
			state.x_phi.x, state.x_phi.y, state.x_theta.x, state.x_theta.y, mpc_info.solvetime);
}
