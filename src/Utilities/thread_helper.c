/* Off-board controller
 * thread_helper.c
 *
 *  Created on: Mar 8, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Functions that help launch and close NUM_THREADS number of threads. 
 */

#include "thread_helper.h"
#include <stdio.h>
#include <stdlib.h>

pthread_t threadID[NUM_THREADS];

int launch_threads(){
	int res,i;
	printf("Launching threads....\n");
	for(i = 0; i < NUM_THREADS; i++){
		res = pthread_create(&threadID[i], NULL, thread_handler[i],NULL);
		if (res != 0){
				printf("Error creating thread %d\n",i+1);
				exit(EXIT_FAILURE);
			}
	}
	printf("Threads launched successfully !\n");
	return EXIT_SUCCESS;
}

int close_threads(){
	int res,i;
	printf("Waiting for threads to join...\n");
	for(i = 0; i < NUM_THREADS; i++){
		res = pthread_join(threadID[i],NULL);
		if (res == 0){
			printf("Thread %d joined successfully ! \n", i+1);
		}
	}
	printf("All threads joined\n");
	return EXIT_SUCCESS;
}






