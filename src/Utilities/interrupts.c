/* Off-board controller
 * interrupts.c
 *
 *  Created on: February, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Signal interfaces 
 */

#include "interrupts.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int register_interrupt(int sig, void (*signal_handler)()){
	signal(sig, signal_handler);
	return EXIT_SUCCESS;
}

int clear_interrupt(int sig){
	signal(sig, SIG_IGN);
	return EXIT_SUCCESS;
}



