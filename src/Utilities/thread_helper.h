/* Off-board controller
 * thread_helper.h
 *
 *  Created on: Mar 8, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Functions that help launch and close NUM_THREADS number of threads. 
 */

#ifndef THREAD_HELPER_H_
#define THREAD_HELPER_H_


#include <pthread.h>
#include "../parameters.h"

typedef void *(*pthread_handler_t)();

extern pthread_t threadID[NUM_THREADS];
extern pthread_handler_t thread_handler[NUM_THREADS];


int launch_threads(void);
int close_threads(void);


#endif /* THREAD_HELPER_H_ */
