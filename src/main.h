///*
// * main.h
// *
// *  Created on: Feb 27, 2015
// *      Author: Praveen Jain
// */
//


#ifndef MAIN_H_
#define MAIN_H_
#include <stdbool.h>

int init_main(void);
int close_main(void);

extern volatile bool exit_flag;
extern volatile bool event_flag;

#endif /* MAIN_H_ */
