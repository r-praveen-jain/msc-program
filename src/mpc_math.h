/*
 * mpc_math.h
 *
 *  Created on: Apr 1, 2015
 *      Author: praveen
 */

/* Acknowledgements: Math operations taken from Paparazzi software*/

#ifndef MPC_MATH_H_
#define MPC_MATH_H_

typedef struct DoubleVect3{
	double x;
	double y;
	double z;
} DoubleVect3;

typedef struct DoubleVect2{
	double x;
	double y;
} DoubleVect2;

typedef struct DoubleEulers{
	double phi;			// Roll
	double theta;		// Pitch
	double psi;			// Yaw
}DoubleEulers;

typedef struct DoubleRates{
	double p;
	double q;
	double r;
}DoubleRates;

// Store the load swing angles
typedef struct DoubleS2{
	double phi;
	double theta;
}DoubleS2;

/*
 * Dimension 3 vectors operations
 */

/* a =  {x, y, z} */
#define VECT3_ASSIGN(_a, _x, _y, _z) {    \
    (_a).x = (_x);        \
    (_a).y = (_y);        \
    (_a).z = (_z);        \
  }

/* a = b */
#define VECT3_COPY(_a, _b) {        \
    (_a).x = (_b).x;        \
    (_a).y = (_b).y;        \
    (_a).z = (_b).z;        \
  }

/* a += b */
#define VECT3_ADD(_a, _b) {     \
    (_a).x += (_b).x;       \
    (_a).y += (_b).y;       \
    (_a).z += (_b).z;       \
  }

/* a -= b */
#define VECT3_SUB(_a, _b) {     \
    (_a).x -= (_b).x;       \
    (_a).y -= (_b).y;       \
    (_a).z -= (_b).z;       \
  }

/* c = a + b */
#define VECT3_SUM(_c, _a, _b) {                 \
    (_c).x = (_a).x + (_b).x;     \
    (_c).y = (_a).y + (_b).y;     \
    (_c).z = (_a).z + (_b).z;     \
  }

/* c = a - b */
#define VECT3_DIFF(_c, _a, _b) {                \
    (_c).x = (_a).x - (_b).x;     \
    (_c).y = (_a).y - (_b).y;     \
    (_c).z = (_a).z - (_b).z;     \
  }

#define BOUND(_a, _max, _min){  \
	if((_a) > (_max)) (_a) = (_max); else if ((_a) < (_min))  (_a) = (_min); \
}

#endif /* MPC_MATH_H_ */
