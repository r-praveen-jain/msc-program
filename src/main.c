//============================================================================
// Name        : main.c
// Author      : Praveen Jain, MSc Systems and Control (r.praveen.jain@gmail.com)
// Year		   : 2015
// Copyright   : Your copyright notice
// Description : Main code for implementation of off-board controller
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include "main.h"
#include "parameters.h"
#include "Communication/Control_COMport.h"
#include "Timer/timing.h"
#include "Utilities/interrupts.h"
#include "Utilities/kbhit.h"
#include "Utilities/file_logger.h"
#include "thread_callbacks.h"
#include "Utilities/thread_helper.h"
#include "Controller/Controller.h"
#include "Controller/KalmanFilter.h"
#include "statemachine.h"
#include <time.h>


// Global variables
volatile bool exit_flag = 0;		// indicate all threads to exit
volatile bool event_flag = 0;		// indicate lapse of sampling period (Ts)
bool run_once = 1;
int seq = 0;						// serial number for control commands
unsigned int timeCounter = 0;		// number of samples k with absolute time being kTs (Ts = sampling period)
uint8_t waypoint_counter = 0;		


// State machine variables
SM_STATE sm_state = STATE_OFF;
SM_EVENT sm_event = EVENT_NIL;
SM_TRANSITION sm_transition_table [NUM_SM_EVENTS][NUM_SM_STATES] = {
							/*OFF*/					/*Idle*/					/*Hover*/						/*Automatic*/            	/*Manual*/
		/*NIL*/			{{STATE_OFF,SM_Null},   {STATE_IDLE,SM_Null},		{STATE_HOVER,SM_Null},  		{STATE_AUTO,SM_Null},		{STATE_MAN,SM_Null}},
		/*INIT*/		{{STATE_IDLE, SM_Init},	{STATE_IDLE,SM_Null},		{STATE_HOVER,SM_Null},  		{STATE_AUTO,SM_Null},		{STATE_MAN,SM_Null}},
		/*Takeoff*/		{{STATE_OFF,SM_Null},	{STATE_HOVER,SM_Takeoff}, 	{STATE_HOVER,SM_Null},			{STATE_AUTO,SM_Null},		{STATE_MAN,SM_Null}},
		/*Man*/			{{STATE_OFF,SM_Null},	{STATE_IDLE,SM_Null},		{STATE_MAN, SM_Hover2Man},		{STATE_MAN,SM_Auto2Man},	{STATE_MAN,SM_Null}},
		/*Auto*/		{{STATE_OFF,SM_Null},	{STATE_IDLE,SM_Null},		{STATE_AUTO,SM_Hover2Auto},		{STATE_AUTO,SM_Null},		{STATE_AUTO,SM_Man2Auto}},
		/*Land*/		{{STATE_OFF,SM_Null},	{STATE_IDLE,SM_land},		{STATE_IDLE,SM_land},			{STATE_IDLE,SM_land},		{STATE_IDLE,SM_land}},
		/*Kill*/		{{STATE_OFF,SM_Null},	{STATE_OFF, SM_Kill},		{STATE_HOVER,SM_Null},			{STATE_AUTO,SM_Null},		{STATE_MAN,SM_Null}}
};


void SIGALRM_handler(){
	event_flag = 1;
	timeCounter++;
}

void SIGINT_handler(){
	sm_event = EVENT_LAND;
}

int main(){
	init_main();
	char ch;
	double cmd[4] = {0};
	double thrust = FMIN;  	// in newtons
	double phi = 0;			// in Degrees
	double theta = 0;		// in Degrees
	unsigned char motors_on = 0;
	while(!exit_flag){
		if(event_flag == 1){
			if(kbhit()){
				ch = readchar();
				if(sm_state == STATE_OFF || sm_state == STATE_IDLE || sm_state == STATE_HOVER || sm_state == STATE_AUTO){
					switch(ch){
					case '0':
						sm_event = EVENT_NIL;
						break;
					case '1':
						sm_event = EVENT_INIT;
						break;
					case '2':
						sm_event = EVENT_TAKEOFF;
						break;
					case '3':
						sm_event = EVENT_MAN;
						break;
					case '4':
						sm_event = EVENT_AUTO;
						break;
					case '5':
						sm_event = EVENT_LAND;
						break;
					case '6':
						sm_event = EVENT_KILL;
						break;
					default:
						break;
					}
				} else if(sm_state == STATE_MAN){
					switch(ch){
					case 'k':
						motors_on = ~(motors_on);
						SetMotorsOn(motors_on);
						break;
					case 'z':
						pos_ref.z += 0.10;
						if(pos_ref.z >= ZMAX){
							pos_ref.z = ZMAX;
						}
//						thrust = thrust + 0.05;
//						if (thrust >= 5){thrust = 5;}
//						else if (thrust <= FMIN){thrust = FMIN;}
//						cmd[0] = thrust; cmd[1] = DEG2RAD(phi); cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 'x':
						pos_ref.z -= 0.10;
						if(pos_ref.z <= ZMIN){
							pos_ref.z = ZMIN;
						}
//						thrust = thrust - 0.05;
//						if (thrust >= 5){thrust = 5;}
//						else if (thrust <= FMIN){thrust = FMIN;}
//						cmd[0] = thrust; cmd[1] = DEG2RAD(phi); cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 'w':
						pos_ref.x += 0.10;
						if(pos_ref.x >= XMAX){
							pos_ref.x = XMAX;
						}
//						theta = theta + 1;
//						if (theta >= 5){theta = 5;}
//						else if (theta <= -5){theta = -5;}
//						cmd[0] = thrust;
//						cmd[1] = DEG2RAD(phi);
//						cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 's':
						pos_ref.x -= 0.10;
						if(pos_ref.x <= XMIN){
							pos_ref.x = XMIN;
						}
//						theta = theta - 1;
//						if (theta >= 5){theta = 5;}
//						else if (theta <= -5){theta = -5;}
//						cmd[0] = thrust;
//						cmd[1] = DEG2RAD(phi);
//						cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 'a':
						pos_ref.y += 0.10;
						if(pos_ref.y >= YMAX){
							pos_ref.y = YMAX;
						}
//						phi = phi + 1;
//						if (phi >= 5){phi = 5;}
//						else if (phi <= -5){phi = -5;}
//						cmd[0] = thrust;
//						cmd[1] = DEG2RAD(phi);
//						cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 'd':
						pos_ref.y -= 0.10;
						if(pos_ref.y <= YMIN){
							pos_ref.y = YMIN;
						}
//						phi = phi - 1;
//						if (phi >= 5){phi = 5;}
//						else if (phi <= -5){phi = -5;}
//						cmd[0] = thrust;
//						cmd[1] = DEG2RAD(phi);
//						cmd[2] = DEG2RAD(theta);
//						SendControlCommand(cmd);
						break;
					case 't':
						sm_event = EVENT_AUTO;
						break;
					case 'l':
						sm_event = EVENT_LAND;
						break;
					case 'n':
						VECT3_COPY(pos_ref, waypoints[waypoint_counter]);
						waypoint_counter++;
						if(waypoint_counter > (NB_WAYPOINTS - 1)){
							waypoint_counter = NB_WAYPOINTS - 1;
						}
						break;
					default:
						break;
					}	// End ---- switch case - manual mode
				}		// End ---- else if(sm_state == STATE_AUTO)
			} 			// End ---- if(kbhit())
			/* Execute state machine continuously*/
			sm_transition_table[sm_event][sm_state].action();
			sm_state = sm_transition_table[sm_event][sm_state].next_sm_state;
		}
	}
	close_main();
	return EXIT_SUCCESS;
}

int init_main(){
	printf("Initializing the Main application...\n");
	init_keyboard();
	init_controller();
	init_timer();
	init_socket();
	launch_threads();
	register_interrupt(SIGALRM, SIGALRM_handler);
	register_interrupt(SIGINT, SIGINT_handler);
	file_logger_start();
	set_timer();
	return EXIT_SUCCESS;
}

int close_main(){
	printf("Closing Main application... \n");
	clear_kf();
	close_keyboard();
	clear_interrupt(SIGALRM);
	clear_interrupt(SIGINT);
	close_timer();
	file_logger_stop();
	close_socket();
	close_threads();
	pthread_rwlock_destroy(&state_rwlock);
	printf("Exiting Main!\n");
	return EXIT_SUCCESS;
}

/* State Machine functions definitions */
void SM_Init(){
	printf("Initializing.....\n");
	// Set reference position of the drone as the default position
	pos_ref.x = pos.x;
	pos_ref.y = pos.y;
	pos_ref.z = pos.z;
	SetMotorsOn(ON);
	//	sm_event = EVENT_NIL;
}

void SM_Takeoff(){
	printf("Taking off....\n");
	pos_ref.x += 0;
	pos_ref.y += 0;
	pos_ref.z = HOVER_ALTITUDE;
	//	sm_event = EVENT_NIL;
}

void SM_Hover2Auto(){
	printf("Hover ---> Auto\n");
	pos_ref.x = pos.x;
	pos_ref.y = pos.y;
	pos_ref.z = HOVER_ALTITUDE;
}

void SM_Hover2Man(){
	printf("Hover ---> Manual\n");
	pos_ref.x = pos.x;
	pos_ref.y = pos.y;
	pos_ref.z = HOVER_ALTITUDE;

}

void SM_Auto2Man(){
	printf("Transition to Manual....\n");
	// Set reference position of the drone to the current position
	pos_ref.x = pos.x;
	pos_ref.y = pos.y;
	pos_ref.z = pos.z;
	//	sm_event = EVENT_NIL;
}

void SM_Man2Auto(){
	printf("Transition to Automatic....\n");
	//	sm_event = EVENT_NIL;
	// Set reference position of the drone to the current position
	pos_ref.x = pos.x;
	pos_ref.y = pos.y;
	pos_ref.z = pos.z;
	//TODO: Resume some kind of flight plan
}
void SM_land(){
	printf("Landing....\n");
	SendLandCommand();
	//	sm_event = EVENT_NIL;
}
void SM_Kill(){
	printf("Killing myself!!!!\n");
	SetMotorsOn(OFF);
	exit_flag = 1;
	//	sm_event = EVENT_NIL;
}

void SM_Null(){
	// Do nothing
	;
}


