/* Off-board controller
 * Control_COMport.h
 *
 *  Created on: February, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Initialize communication with the drone
 */

#ifndef CONTROL_COMPORT_H_
#define CONTROL_COMPORT_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "../parameters.h"


#define DOMAIN 			AF_INET			// used in socket() function call
#define TYPE 			SOCK_DGRAM		// used in socket() function call
#define PROTOCOL 		0				// used in socket() function call
#define BACKLOG			5				// used in listen() function call
#define MAX_PACKET_SIZE 1024

typedef struct socket_t {
	int sockfd;
	struct sockaddr_in addr;
	socklen_t addr_len;
} socket_t;


// Global Variables
extern socket_t laptop_control, drone_control;		// Sockets used to send control commands

#ifdef USE_TELEMETRY
extern socket_t laptop_telemetry, drone_telemetry;	// Sockets used to receive telemetry data
#endif

int init_socket(void);
int SendPacket(unsigned char *packet, unsigned char numBytes);
int close_socket(void);
int ReceiveTelemetry(void);


#endif /* CONTROL_COMPORT_H_ */
