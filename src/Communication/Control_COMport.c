/* Off-board controller
 * Control_COMport.c
 *
 *  Created on: February, 2015
 *      Author: Praveen Jain (r.praveen.jain@gmail.com)
 * Description: Initialize communication with the drone
 */

#include "Control_COMport.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


// Global Variables
socket_t laptop_control, drone_control;

#ifdef USE_TELEMETRY
socket_t laptop_telemetry, drone_telemetry;
#endif

int init_socket(){
	printf("Initializing Sockets... \n");
	char res;

	laptop_control.addr.sin_family = DOMAIN;
	inet_aton(LAPTOP_IP, &laptop_control.addr.sin_addr);
	laptop_control.addr.sin_port = htons(CONTROL_PORT);
	laptop_control.addr_len = sizeof(laptop_control.addr);

	drone_control.addr.sin_family = DOMAIN;
	inet_aton(DRONE_IP, &drone_control.addr.sin_addr);
	drone_control.addr.sin_port = htons(CONTROL_PORT);
	drone_control.addr_len = sizeof(drone_control.addr);

	laptop_control.sockfd = socket(DOMAIN,TYPE,PROTOCOL);
	res =  bind(laptop_control.sockfd, (struct sockaddr *) &laptop_control.addr, laptop_control.addr_len);
	if (res == -1){
		perror("Socket binding failed");
		exit(EXIT_FAILURE);
	}

#ifdef USE_TELEMETRY
	laptop_telemetry.addr.sin_family = DOMAIN;
	inet_aton(LAPTOP_IP, &laptop_telemetry.addr.sin_addr);
	laptop_telemetry.addr.sin_port = htons(TELEMETRY_PORT);
	laptop_telemetry.addr_len = sizeof(laptop_telemetry.addr);

	drone_telemetry.addr.sin_family = DOMAIN;
	inet_aton(DRONE_IP, &drone_telemetry.addr.sin_addr);
	drone_telemetry.addr.sin_port = htons(TELEMETRY_PORT);
	drone_telemetry.addr_len = sizeof(drone_telemetry.addr);

	laptop_telemetry.sockfd = socket(DOMAIN,TYPE,PROTOCOL);
	res =  bind(laptop_telemetry.sockfd, (struct sockaddr *) &laptop_telemetry.addr, laptop_telemetry.addr_len);
	if (res == -1){
		perror("Socket binding failed");
		exit(EXIT_FAILURE);
	}
#endif

	printf("Sockets Initialized !\n");
	return EXIT_SUCCESS;
}


int SendPacket(unsigned char *packet, unsigned char numBytes){
	char n;
	unsigned char tempbuf[numBytes];
	memcpy(tempbuf, packet, numBytes);
#ifdef PRINT_DEBUG_INFO
	unsigned short thrust = ((tempbuf[4]<<8) | tempbuf[5]);
	short phi = ((tempbuf[6]<<8) | tempbuf[7]);
	short theta = ((tempbuf[8]<<8) | tempbuf[9]);
	short psi = ((tempbuf[10]<<8) | tempbuf[11]);
	printf("%d %d %d %d %d %d %d\n",(unsigned char)tempbuf[0],
			(unsigned char)tempbuf[1],
			(unsigned int)((tempbuf[2]<<8) | tempbuf[3]),
			thrust,
			phi,
			theta, psi);
#endif
	n = sendto(laptop_control.sockfd, packet, numBytes,0, (struct sockaddr *)&drone_control.addr, drone_control.addr_len);
	if (n < 0){
		perror("Could not send control command\n");
		exit(EXIT_FAILURE);
	}
	return n;
}

int close_socket(){
	printf("Closing Sockets...\n");
	char res;
	res = close(laptop_control.sockfd);
	if(res == -1){
		perror("Error Closing Socket");
		exit(EXIT_FAILURE);
	}
#ifdef USE_TELEMETRY
	res = close(laptop_telemetry.sockfd);
	if(res == -1){
		perror("Error Closing Socket");
		exit(EXIT_FAILURE);
	}
#endif
	printf("Sockets Closed! \n");
	return EXIT_SUCCESS;
}
