/*
 * thread_callbacks.c
 *
 *  Created on: Mar 9, 2015
 *      Author: praveen
 */

#include "thread_callbacks.h"
#include "Utilities/thread_helper.h"
#include "Timer/timing.h"
#include "Utilities/kbhit.h"
#include "Utilities/file_logger.h"
#include "Controller/Controller.h"
#include "Communication/Control_COMport.h"
#include "statemachine.h"
#include "parameters.h"
#include "mpc_math.h"
#include "state.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <ivy.h>
#include <ivyloop.h>
#include <math.h>

//#########################################################################
// Globals needed for the execution of threads
//#########################################################################

char *ivy_bus = "127.255.255.255:2010"; // Needed for optitrack thread
pthread_rwlock_t  state_rwlock = PTHREAD_RWLOCK_INITIALIZER;

char telemetry_packet[1024] = {0};
state_t state;
volatile bool low_bat = 0;

//#########################################################################
// THREAD 1: Implementation of the Estimator and Controller
//#########################################################################
void *Controller_thread(){
	printf("Controller Thread started !\n");
#ifdef USE_STATE_MACHINE
	while(!exit_flag){
		if(event_flag == 1){
			switch(sm_state){
			case STATE_OFF:
				break;
			case STATE_IDLE:
				break;
			case STATE_HOVER:
				hover_controller();
				SendControlCommand(ControlCommand);
				break;
			case STATE_AUTO:
				pthread_rwlock_rdlock(&state_rwlock);
				estimate_state();
				pthread_rwlock_unlock(&state_rwlock);
				execute_controller();
				SendControlCommand(ControlCommand);
				break;
			case STATE_MAN:
				pthread_rwlock_rdlock(&state_rwlock);
				estimate_state();
				pthread_rwlock_unlock(&state_rwlock);
				execute_controller();
				SendControlCommand(ControlCommand);
				break;
			default:
				break;
			}
			file_logger_periodic();
			printf("mag: %f opti: %f bat: %f\n", RAD2DEG(state.att.psi), RAD2DEG(state.optitrack_heading), state.vsupply);
//			printf("phi: %f  theta: %f psi: %f bat: %f\n",RAD2DEG(state.att.phi),RAD2DEG(state.att.theta), RAD2DEG(state.att.psi), state.vsupply);
			event_flag = 0;
		}
	}
#endif
	IvySendMsg("Bye");
	pthread_exit(NULL);
}

//#########################################################################
// THREAD 2: Access measurements from Optitrack system
//#########################################################################
static int parse_MPC_GPSmessages(char *arg){
	char *ptr;
	double x[9];
	int i = 0;
	char *saveptr;
	ptr = strtok_r(arg, ",", &saveptr);
	if(ptr != NULL){
		do {
			x[i] = atof(ptr);
			i++;
			ptr = strtok_r(NULL, ",", &saveptr);
		} while(ptr != NULL);
	}
	// Convert from mm (data coming in from natnet2ivy application) to m (required by MPC and PID)
	pos.x = x[2]/1000; pos.y = x[3]/1000; pos.z = x[4]/1000;
	vel.x = x[5]/1000; vel.y = x[6]/1000; vel.z = x[7]/1000;
	state.optitrack_heading = x[8]/10000000.0;
	state.measure_flag = 1;

#ifdef PRINT_DEBUG_INFO
//	printf("Read %f %f %f %f %f %f\n", pos.x, pos.y, pos.z, vel.x, vel.y, vel.z);
#endif
	return EXIT_SUCCESS;
}

// Calls associated with the IVYBUS
/* callback associated to "Hello" messages */
void MPC_GPSCallback (IvyClientPtr app, void *data, int argc, char **argv)
{
	char* arg = (argc < 1) ? "" : argv[0];

	pthread_rwlock_wrlock(&state_rwlock);
	parse_MPC_GPSmessages(arg);
	pthread_rwlock_unlock(&state_rwlock);
}

// Calls associated with the IVYBUS
/* callback associated to "Bye" messages */
void ByeCallback (IvyClientPtr app, void *data, int argc, char **argv)
{
	IvyStop();
}

// Thread Callback
void *Optitrack_thread(void){
	printf("Starting the Optitrack thread\n");

	IvyInit ("Offboard_Controller", "Hello world", 0, 0, 0, 0);
	IvyStart (ivy_bus);

	/* binding of Callback to messages starting with '0 REMOTE_GPS' */
	// This data comes from another process natnet2ivy
	IvyBindMsg (MPC_GPSCallback, 0, "^MPC_GPS(.*)");

	/* binding of ByeCallback to 'Bye' */
	IvyBindMsg (ByeCallback, 0, "^Bye$");

	/* main loop */
	IvyMainLoop();
	pthread_exit(NULL);
}

//#########################################################################
// THREAD 3: Telemetry thread - get and parse the data from the drone
//#########################################################################
static int parseTelemetryData(char *packet){
	char *ptr, *saveptr;
	int i = 0;
	double x[13];
	ptr = strtok_r(packet, ",", &saveptr);
	if(ptr != NULL){
		do {
			x[i] = atof(ptr);
			i++;
			ptr = strtok_r(NULL, ",", &saveptr);
		} while(ptr != NULL);
	}

	state.vsupply 			= x[0]/10.0; // Decivolts to volts conversion
	state.stab_cmd.phi 		= x[1];
	state.stab_cmd.theta 	= x[2];
	state.stab_cmd.psi 		= x[3];
	state.att_sp.phi 		= x[4];
	state.att_sp.theta		= x[5];
	state.att_sp.psi		= x[6];
	state.att.phi			= x[7];
	state.att.theta			= x[8];
	state.att.psi			= x[9];
	state.rate.p 			= x[10];
	state.rate.q			= x[11];
	state.rate.r 			= x[12];

	return EXIT_SUCCESS;
}

void *telemetry_thread(){
	uint16_t n;
	while(!exit_flag){
		n = recvfrom(laptop_telemetry.sockfd, telemetry_packet, 1024,0, (struct sockaddr *)&drone_telemetry.addr, &drone_telemetry.addr_len);
		if (n < 0){
			perror("Could not receive control command\n");
			exit(EXIT_FAILURE);
		}
		pthread_rwlock_wrlock(&state_rwlock);
		parseTelemetryData(telemetry_packet);
		pthread_rwlock_unlock(&state_rwlock);
		//file_logger_periodic();
//		printf("%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
//						 state.vsupply,
//						 state.stab_cmd.phi,
//						 state.stab_cmd.theta,
//						 state.stab_cmd.psi,
//						 state.att_sp.phi,
//						 state.att_sp.theta,
//						 state.att_sp.psi,
//						 state.att.phi,
//						 state.att.theta,
//		 				 state.att.psi,
//						 state.rate.p,
//						 state.rate.q,
//					     state.rate.r);
	}
	pthread_exit(NULL);
}

//---------------------------------------------------------------------------------------------------
// Add the thread callbacks to the thread_handler array
//---------------------------------------------------------------------------------------------------

pthread_handler_t thread_handler[NUM_THREADS] = {Controller_thread, Optitrack_thread, telemetry_thread};

